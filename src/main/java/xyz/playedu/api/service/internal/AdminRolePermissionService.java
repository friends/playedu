/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.service.internal;

import com.baomidou.mybatisplus.extension.service.IService;

import xyz.playedu.api.domain.AdminRolePermission;

/**
 * @author tengteng
 * @description 针对表【admin_role_permission】的数据库操作Service
 * @createDate 2023-02-21 16:07:01
 */
public interface AdminRolePermissionService extends IService<AdminRolePermission> {}
