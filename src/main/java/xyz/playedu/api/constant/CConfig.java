/**
 * This file is part of the PlayEdu.
 * (c) 杭州白书科技有限公司
 */
package xyz.playedu.api.constant;

/**
 * @Author 杭州白书科技有限公司
 *
 * @create 2023/4/11 10:12
 */
public class CConfig {

    public static final String SYSTEM_NAME = "system.name";
    public static final String SYSTEM_LOGO = "system.logo";
    public static final String SYSTEM_API_URL = "system.api_url";
    public static final String SYSTEM_PC_URL = "system.pc_url";
    public static final String SYSTEM_H5_URL = "system.h5_url";

    public static final String MEMBER_DEFAULT_AVATAR = "member.default_avatar";
}
